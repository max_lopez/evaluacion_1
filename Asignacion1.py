import sqlite3

conexion = sqlite3.connect("Palabras")
cursor = conexion.cursor()

# creación de la tabla y sus campos
"""cursor.execute(''' 
    CREATE TABLE PALABRAS (
        ID INTEGER PRIMARY KEY AUTOINCREMENT, 
        PALABRA VARCHAR(50) UNIQUE,
        SIGNIFICADO VARCHAR(200) UNIQUE 
    )
''')"""

# Agregar nueva palabra con su significado, ninguna palabra y significado se puede repetir de lo contrario devolvera un error

lista = [

    ("BULTO", "Un bulto es una persona que es mala, realmente mala en lo que se desempeña"),
    ("CHANIAO", "cuando una persona se arregla o se viste mejor de lo habitual se le dice que está chaniao. Esta palabra proviene del inglés shining")

]

cursor.executemany("INSERT INTO PALABRAS VALUES (NULL,?,?)", lista)

# Editar palabra existente por medio del numero id
cursor.execute("UPDATE PALABRAS SET PALABRA='QUEXOPA' WHERE ID = 1")

"""# Eliminar palabra existente por medio del numero id
cursor.execute("DELETE FROM PALABRAS WHERE ID = 4")"""

# Ver listado de palabras
cursor.execute("SELECT * FROM PALABRAS")

VariasPalabras = cursor.fetchall()

print("\n")
print("PALABRA" + "\t\t" + "SIGNIFICADO")
for palabra in VariasPalabras:
    print(palabra[1], ":\t\t", palabra[2])

# Buscar significado de palabra por medio de la palabra
cursor.execute("SELECT * FROM PALABRAS WHERE PALABRA = 'QUEXOPA'")
signficado = cursor.fetchall()
print(signficado)

conexion.commit()

conexion.close()
